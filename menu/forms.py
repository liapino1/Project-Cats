from django import forms
from menu.models import Cat

class CreateCatForm(forms.ModelForm):
    class Meta:
        model = Cat
        fields = ['name', 'age', 'food', 'image', 'state_morning', 'state_afternoon', 'state_evening']
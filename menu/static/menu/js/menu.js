
$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      enctype: 'multipart/form-data',
      beforeSend: function () {
        $("#modal-cat .modal-content").html("");
        $("#modal-cat").modal("show");
      },
      success: function (data) {
        $("#modal-cat .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this); 
    /* var form = new FormData($(this)[0]); */
    var fd = new FormData($(this)[0]);
    
    $.ajax({
      url: form.attr("action"),
      data: fd,
      type: form.attr("method"),
      dataType: 'json',
      processData: false,
      contentType: false,
      
      success: function (data) {
          
          if (data.form_is_valid) {
            console.log(data);
            $("#cat-table tbody").html(data.html_cat_list);
            $("#modal-cat").modal("hide");
            
            swal("Cat added successfully!", "", "success")
            
          }
          else {
            $("#modal-cat .modal-content").html(data.html_form);
          }
        
      }
    });
    return false;
  };


  /* Binding */

  // Create book
  $(".js-create-cat").click(loadForm);
  $("#modal-cat").on("submit", ".js-cat-create-form", saveForm);

  // Update book
  $("#cat-table").on("click", ".js-update-cat", loadForm);
  $("#modal-cat").on("submit", ".js-cat-update-form", saveForm);

  // Delete book
  $("#cat-table").on("click", ".js-delete-cat", loadForm);
  $("#modal-cat").on("submit", ".js-cat-delete-form", saveForm);

});

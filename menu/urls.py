from django.urls import path
from menu.views import cat_create, cat_list, cat_update, cat_delete

app_name = 'menu'

urlpatterns = [
    path('cats/', cat_list, name="cat_list"),
    path('cats/cat_create', cat_create, name="cat_create"),
    path('cats/cat_update/<int:pk>/', cat_update, name="cat_update"),
    path('cats/cat_delete/<int:pk>/', cat_delete, name="cat_delete"),
]
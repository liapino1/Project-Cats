from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse, HttpResponse
from menu.models import Cat
from menu.forms import CreateCatForm


def cat_list(request):
    template_name = 'menu/cat_list.html'
    cats = Cat.objects.all()
    return render(request, template_name, {'cats': cats})


def cat_create(request):
    if request.method == 'POST':
        form = CreateCatForm(request.POST or None, request.FILES or None)
    else:
        form = CreateCatForm()
    return save_cat_form(request, form, 'menu/partial_cat_create.html')

def save_cat_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            cats = Cat.objects.all()
            data['html_cat_list'] = render_to_string('menu/partial_cat_list.html', {
                'cats': cats
            })

        else:
            data['form_is_valid'] = False

    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def cat_update(request, pk):
    cat = get_object_or_404(Cat, pk=pk)
    if request.method == 'POST':
        form = CreateCatForm(request.POST, instance=cat)
    else:
        form = CreateCatForm(instance=cat)
    return save_cat_form(request, form, 'menu/partial_cat_update.html')


def cat_delete(request, pk):
    cat = get_object_or_404(Cat, pk=pk)
    data = dict()
    if request.method == 'POST':
        cat.delete()
        data['form_is_valid'] = True
        cats = Cat.objects.all()
        data['html_cat_list'] = render_to_string('menu/partial_cat_list.html', {
            'cats': cats
        })
    else:
        context = {'cat': cat}
        data['html_form'] = render_to_string('menu/partial_cat_delete.html', context, request=request)
    return JsonResponse(data)





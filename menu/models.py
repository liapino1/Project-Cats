from django.db import models


class Cat(models.Model):
    name = models.CharField(max_length=20)
    age = models.PositiveIntegerField(default=0)
    food = models.CharField(max_length=20)
    image = models.FileField(upload_to='media')
    state_morning = models.BooleanField(blank=True)
    state_afternoon = models.BooleanField(blank=True)
    state_evening = models.BooleanField(blank=True)

    def __str__(self):
        return self.name


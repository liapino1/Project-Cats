from django.urls import path
from authentication import views as l_views

app_name = 'log'

urlpatterns = [
    path('', l_views.login, name='login_login'),
    path('logout/', l_views.logout, name='login_logout'),
    path('register/', l_views.register, name='login_register'),
    path('dashboard/', l_views.dashboard, name='login_dashboard'),

]
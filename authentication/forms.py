from django import forms
from .models import UserProfile

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        labels = {
            'username': 'Nombre usuario',
            'email': 'Correo',
            'password': 'Contraseña',
            'address': 'Dirección',
        }
        
        widgets = {'address':forms.TextInput(attrs={'class':'form-control col-xs-12', 'placeholder':'Address'}),
                    }

        exclude = ('user','birthdate','gender')
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate
from django.contrib.auth import login as login_user
from django.contrib import messages
from .forms import UserProfileForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, logout as auth_logout, login as auth_login


def login(request):
    if request.POST:
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user.is_staff:
            login_user(request, user)
            #messages.success(request, "Ya esta registrado)
            return HttpResponseRedirect(reverse('menu:list_menu'))
        elif user.is_active:
            login_user(request, user)
            return HttpResponseRedirect(reverse('menu:list_orders'))
        else:
            messages.warning(request, "El usuario no existe")
        return HttpResponseRedirect(reverse('log:login_login'))
    return render(request, 'login.html',{})

def logout(request):
    #return render(request, 'logout.html',{})
    auth_logout(request)
    return HttpResponseRedirect(reverse('log:login_login'))

@login_required(login_url='/register/')
def dashboard(request):
    return render(request, 'dashboard.html',{})

def register(request):
    if request.POST:
        form = UserProfileForm(request.POST)

        if form.is_valid() and not User.objects.filter(username=request.POST['username']).exists():
            user = User.objects.create_user(username=request.POST['username'],
                                            email=request.POST['email'],
                                            password=request.POST['password'])
            form = form.save(commit=False)
            form.user = user
            form.save()
            messages.success(request, "Usuario registrado")
            return HttpResponseRedirect(reverse('log:login_login'))
    else:
        form = UserProfileForm()
    return render(request, 'register.html', {'form':form})
